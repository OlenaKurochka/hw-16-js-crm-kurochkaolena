import { modalHide, modalShow, createInputsForModal, generationID } from "./functions.js";


const btnShowMadal = document.querySelector(".add"),
    btnCloseModal = document.getElementById("close"),
    btnSaveModal = document.getElementById("save"),
    select = document.getElementById("select"),
    formInfo = document.querySelector(".form-info"),
    store = ["Назва продукту", "Вартість продукту", "Посилання на зображення", "Опис продукту", "Ключеві слова (Розділяти комою)"];

let typeCategory = null;
const video = ["ID", "Назва", "Посилання", "Жанр (Розділяти комою)", "Країна (Розділяти комою)", "Актори (Розділяти комою)", "Опис", "Рік виходу"];
const restaurant = ["ID","Назва","Вага","Інгрідієнти (Розділяти комою)","Ціна","Зображження","Ключеві слова (Розділяти комою)","Фактична вага","Оцінка"]

btnShowMadal.addEventListener("click", modalShow);
btnCloseModal.addEventListener("click", modalHide);

select.addEventListener("change", () => {
    typeCategory = select.value;

    while (formInfo.hasChildNodes()) {
        formInfo.removeChild(formInfo.firstChild);
    } 


    if (select.value === "Магазин") {
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(store));
    } else if (select.value === "Відео хостинг") {
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(video));
    } else if (select.value === "Ресторан") {
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(restaurant));
    } else {
        console.error("Жоден з пунктів не валідний.")
        return
    }
})

btnSaveModal.addEventListener("click", () => {
    const [...inputs] = document.querySelectorAll(".form-info input");
    const objStore = {
        id: "",
        status: false,
        productName: "",
        porductPrice: 0,
        productImage: "",
        productDescription: "",
        productQuantity: 0,
        keywords: []
    }
    const objVideo = {
        id: 0,
        videoLink: "",
        videoName: "",
        videoGenre: [],
        videoCountry: [],
        videoActors: [],
        videoDescription: "",
        videoYearReleased: 2023
    }
    const obJRestotation = {
        id: 0,
        productName: "",
        productWeiht: "",
        ingredients: [],
        price: 0,
        productImageUrl: "",
        keywords: [],
        Weiht : 0,
        stopList : false,ageRestrictions : false,
        like : 0 ,
        ageRestrictions : false
    }


    if (typeCategory === "Магазин") {
        objStore.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва продукту") {
                    objStore.productName = input.value;
                } else if (input.dataset.type === "Вартість продукту") {
                    objStore.porductPrice = parseFloat(input.value);
                } else if (input.dataset.type === "Посилання на зображення") {
                    objStore.productImage = input.value;
                } else if (input.dataset.type === "Опис продукту") {
                    objStore.productDescription = input.value;
                } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    objStore.keywords.push(...input.value.split(","))
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objStore.date = new Date();
        if(objStore.productQuantity <= 0){
            objStore.status = false;
        }else{
            objStore.status = true;
        }
        const store = JSON.parse(localStorage.BDStore);
        store.push(objStore);
        localStorage.BDStore = JSON.stringify(store);
    }else if(typeCategory === "Відео хостинг"){
        objVideo.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "ID") {
                    objVideo.id = input.value;
                }else if (input.dataset.type === "Назва") {
                    objVideo.videoName = input.value;
                }else if (input.dataset.type === "Посилання") {
                    objVideo.videoLink = input.value;
                }else if (input.dataset.type === "Жанр (Розділяти комою)") {
                    objVideo.videoGenre.push(...input.value.split(","))
                }else if (input.dataset.type === "Країна (Розділяти комою)") {
                    objVideo.videoCountry.push(...input.value.split(","))
                }else if (input.dataset.type === "Актори (Розділяти комою)") {
                    objVideo.videoActors.push(...input.value.split(","))
                }else if (input.dataset.type === "Опис") {
                    objVideo.videoDescription = input.value;
                }else if (input.dataset.type === "Рік виходу") {
                    objVideo.videoYearReleased = input.value;
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objVideo.date = new Date();
        const video = JSON.parse(localStorage.BDVideo);
        video.push(objVideo);
        localStorage.BDVideo = JSON.stringify(video);

    }else if(typeCategory === "Ресторан"){
        obJRestotation.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "ID") {
                    obJRestotation.id = input.value;
                }else if (input.dataset.type === "Назва") {
                    obJRestotation.productName = input.value;
                }else if (input.dataset.type === "Вага") {
                    obJRestotation.productWeiht = input.value;
                }else if (input.dataset.type === "Інгрідієнти (Розділяти комою)") {
                    obJRestotation.ingredients.push(...input.value.split(","))
                }else if (input.dataset.type === "Ціна") {
                    obJRestotation.price = input.value;
                }else if (input.dataset.type === "Зображження") {
                    obJRestotation.productImageUrl = input.value;
                }else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    obJRestotation.keywords.push(...input.value.split(","))
                }else if (input.dataset.type === "Фактична вага") {
                    obJRestotation.Weiht = input.value;
                }else if (input.dataset.type === "Оцінка") {
                    obJRestotation.like = input.value;
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        obJRestotation.date = new Date();
        if(obJRestotation.ageRestrictions === "false"){
            obJRestotation.status = true;
        }else{
            obJRestotation.status = false;
        }
        const restaurant = JSON.parse(localStorage.BDRest);
        restaurant.push(obJRestotation);
        localStorage.BDRest = JSON.stringify(restaurant);

    }
})

import "./video.js"
import "./restaurant.js"





