
const tbody = document.querySelector("tbody");
const videoStorage = JSON.parse(localStorage.BDVideo);


function createElementTable (videoStorage) {
   return videoStorage.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td title="При настиску сортувати.">${el.videoName}</td>
            <td>${el.videoGenre}</td>
            <td>${el.videoCountry}</td>
            <td>${el.videoActors}</td>
            <td title="При настиску сортувати.">${el.videoYearReleased}</td>
            <td>${el.videoLink}</td>
            <td>&#128397;</td>
            <td>${el.date}</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

try {
    tbody.insertAdjacentHTML("beforeend", createElementTable(videoStorage))
} catch (error) {
    
}


