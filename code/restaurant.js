const tbody = document.querySelector("tbody");
const restaurantStorage = JSON.parse(localStorage.BDRest);

function createElementTable (restaurantStorage) {
   return restaurantStorage.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td title="При настиску сортувати.">${el.productName}</td>
            <td>${el.productWeiht}</td>
            <td>${el.ingredients}</td>
            <td>${el.productImageUr}</td>
            <td title="При настиску сортувати.">${el.price}</td>
            <td>${el.keywords}</td>
            <td>${el.status ? "&#9989;" : "&#10060;" }</td>
            <td>&#128397;</td>
            <td>${el.date}</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

try {
    tbody.insertAdjacentHTML("beforeend", createElementTable(restaurantStorage))
} catch (error) {
    
}
